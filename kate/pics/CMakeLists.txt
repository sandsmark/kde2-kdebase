kde2_icon(hi16-app-kate.png hi32-app-kate.png hi48-app-kate.png)

install(FILES hi16-app-kwrite.png DESTINATION DESTINATION ${KDE2_ICONDIR}/hicolor/16x16 RENAME kwrite2.png)
install(FILES hi32-app-kwrite.png DESTINATION DESTINATION ${KDE2_ICONDIR}/hicolor/32x32 RENAME kwrite2.png)
install(FILES hi48-app-kwrite.png DESTINATION DESTINATION ${KDE2_ICONDIR}/hicolor/48x48 RENAME kwrite2.png)

add_subdirectory(actions)
add_subdirectory(tabicons)
add_subdirectory(indicators)
